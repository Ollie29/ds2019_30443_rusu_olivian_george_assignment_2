package com.example.springdemo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class EndpointsWebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().passwordEncoder(new BCryptPasswordEncoder()).dataSource(dataSource)
                .usersByUsernameQuery("SELECT user_name, pass_word, enabled FROM user_table where user_name = ?")
                .authoritiesByUsernameQuery("SELECT user_name, role FROM user_table WHERE user_name = ?")
                .rolePrefix("ROLE_");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers().disable()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers("/admin/user").hasRole("DOCTOR")
                .antMatchers("/caregiver/**").hasAnyRole("DOCTOR", "CAREGIVER")
                .antMatchers("/patient/**").hasAnyRole("DOCTOR", "PATIENT")
                .antMatchers("/medication/**").hasRole("DOCTOR")
                .antMatchers("/application/login").permitAll()
                .and()
                .httpBasic();
    }

}
