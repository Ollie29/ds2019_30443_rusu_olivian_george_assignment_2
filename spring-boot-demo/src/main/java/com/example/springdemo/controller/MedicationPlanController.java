package com.example.springdemo.controller;


import com.example.springdemo.dto.IntakeIntervalDTO;
import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.IntakeInterval;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.repositories.MedicationPlanRepository;
import com.example.springdemo.services.MedicationPlanService;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication/plan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @PutMapping(value = "/add/intake/{id}")
    public MedicationPlanDTO addIntakeIntervalToMedicationPlan(@PathVariable("id") Integer id){
        return null;
        //TODO
    }

}
