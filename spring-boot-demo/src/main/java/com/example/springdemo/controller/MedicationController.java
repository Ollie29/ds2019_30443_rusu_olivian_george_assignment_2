package com.example.springdemo.controller;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping
    public List<MedicationDTO> findAll() {
        return medicationService.findAll();
    }

    @GetMapping(value = "/id/{id}")
    public MedicationDTO getMedicationById(@PathVariable("id") Integer id) {
        return medicationService.getMedicationById(id);
    }

    @PostMapping(value = "/insert")
    public Integer insertMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.insertMedication(medicationDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.updateMedication(medicationDTO);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteMedication(@PathVariable("id") Integer id){
        medicationService.deleteMedication(id);
    }
}
