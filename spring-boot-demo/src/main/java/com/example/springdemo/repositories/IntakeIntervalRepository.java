package com.example.springdemo.repositories;

import com.example.springdemo.entities.IntakeInterval;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IntakeIntervalRepository extends JpaRepository<IntakeInterval, Integer> {
}
