package com.example.springdemo;

import java.io.Serializable;

public class Activity implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    private String patientId;
    private String activity;
    private long startDate;
    private long endDate;

    public Activity(String patientId, String activity, long startDate, long endDate) {
        this.patientId = patientId;
        this.activity = activity;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "patientId='" + patientId + '\'' +
                ", activity='" + activity + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
