package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.PatientViewBuilder;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private PatientRepository patientRepository;
    private CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public PatientDTO getPatientById(Integer id) {

        Optional<Patient> patient = patientRepository.findById(id);

        if(!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "patient id", id);
        }
        return PatientBuilder.generateDTOFromEntityWithCaregiver(patient.get());
    }

    public List<PatientDTO> getAll() {
        List<Patient> patients = patientRepository.findAll();

        return patients.stream().map(PatientBuilder::generateDTOFromEntityWithCaregiver).collect(Collectors.toList());
    }

    public List<PatientViewDTO> getPatientsByFirstName(String firstName) {

        List<Patient> patients = patientRepository.findAllByFirstName(firstName);
        return patients.stream().map(PatientViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public PatientDTO insertPatient(PatientDTO patientDTO, Integer caregiverID) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverID);
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        Patient patient = PatientBuilder.generateEntityFromDTO(patientDTO);
        if(!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverID);
        }
        patient.setCaregiver(caregiver.get());
        System.out.println(caregiverID);
        patientRepository.save(patient);
        return patientDTO;
    }

    public Integer updatePatient(PatientDTO patientDTO, Integer caregiverID) {
        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverID);
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        Patient patient1 = PatientBuilder.generateEntityFromDTO(patientDTO);

        if(!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", patientDTO.getId().toString());
        }

        if(!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverID);
        }

        patient1.setCaregiver(caregiver.get());
        patientRepository.save(patient1);

        return patient1.getId();
    }

    public void deletePatient(int id) {
        patientRepository.deleteById(id);
    }

}
