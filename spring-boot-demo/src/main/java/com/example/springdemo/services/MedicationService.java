package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.builders.MedicationBuilder;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository repo) {
        this.medicationRepository = repo;
    }

    public List<MedicationDTO> findAll() {
        return medicationRepository.findAll().stream().map(MedicationBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public MedicationDTO getMedicationById(Integer id) {
        Optional<Medication> medication = medicationRepository.findById(id);

        if(!medication.isPresent()){
            throw new ResourceNotFoundException("medication", "medication id", id);
        }

        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public Integer insertMedication(MedicationDTO medicationDTO) {
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public Integer updateMedication(MedicationDTO medicationDTO) {
        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());
        if(!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "Medication id", medicationDTO.getId());
        }

        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void deleteMedication(Integer id) {
        medicationRepository.deleteById(id);
    }

}
