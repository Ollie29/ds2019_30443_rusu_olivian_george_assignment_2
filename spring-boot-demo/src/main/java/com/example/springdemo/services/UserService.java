package com.example.springdemo.services;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.builders.UserBuilder;
import com.example.springdemo.entities.User;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }



    public Integer insertUser(UserDTO userDTO) {

        User user = UserBuilder.generateEntityFromDTO(userDTO);

        PasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassWord(encoder.encode(userDTO.getPassWord()));

        return userRepository.save(user).getId();
    }

    public UserDTO getUserByUserName(String username) {
        User user = userRepository.findByUserName(username);

        return UserBuilder.generateDTOFromEntity(user);
    }


}
