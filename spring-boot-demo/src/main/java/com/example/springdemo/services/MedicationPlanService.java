package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeIntervalDTO;
import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.IntakeInterval;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MedicationPlanService {

    private MedicationPlanRepository medicationPlanRepository;
    private IntakeIntervalService intakeIntervalService;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, IntakeIntervalService intakeIntervalService) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.intakeIntervalService = intakeIntervalService;
    }

    public MedicationPlan addIntakeIntervalToMedicationPlan(Integer medID, Integer medicationPlanID, IntakeIntervalDTO intakeIntervalDTO) {
        IntakeInterval intakeInterval = intakeIntervalService.insertIntakeIntervalForMedication(intakeIntervalDTO, medID);
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanID);
        if(!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("medication plan", "medication plan id", medicationPlanID);
        }
        medicationPlan.get().getIntakeIntervals().add(intakeInterval);
        intakeInterval.setMedicationPlan(medicationPlan.get());

        medicationPlanRepository.save(medicationPlan.get());

        return medicationPlan.get();
    }


}
