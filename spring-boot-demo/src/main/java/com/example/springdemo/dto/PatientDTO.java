package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.enums.GenderType;

public class PatientDTO {

    private Integer id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private GenderType gender;
    private String address;
    private CaregiverDTO caregiver;

    public PatientDTO() {
    }

    public PatientDTO(Integer id, String firstName, String lastName, String birthDate, GenderType gender, String address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public PatientDTO(Integer id, String firstName, String lastName, String birthDate, GenderType gender, String address, CaregiverDTO caregiverDTO) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.caregiver = caregiverDTO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CaregiverDTO getCaregiverDTO() {
        return caregiver;
    }

    public void setCaregiverDTO(CaregiverDTO caregiverDTO) {
        this.caregiver = caregiverDTO;
    }
}
