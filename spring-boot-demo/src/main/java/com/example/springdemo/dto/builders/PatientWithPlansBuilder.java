package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.dto.PatientWithPlansDTO;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class PatientWithPlansBuilder {



    private PatientWithPlansBuilder() {
    }

    public static PatientWithPlansDTO generateDTOFromEntity(Patient patient, List<MedicationPlan> plans) {
        List<MedicationPlanDTO> dtos = plans.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new PatientWithPlansDTO(
                patient.getId(),
                patient.getFirstName(),
                patient.getLastName(),
                dtos
        );
    }
}
