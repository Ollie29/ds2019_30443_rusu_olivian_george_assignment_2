package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.MedicationPlan;

public class MedicationPlanBuilder {

    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medPlan) {
        return new MedicationPlanDTO(
                medPlan.getId(),
                medPlan.getStartDate(),
                medPlan.getEndDate()
        );
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate()
        );
    }
}
