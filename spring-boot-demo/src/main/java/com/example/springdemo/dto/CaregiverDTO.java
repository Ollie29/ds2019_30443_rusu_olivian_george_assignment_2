package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.enums.GenderType;

import java.util.Date;

public class CaregiverDTO {

    private Integer id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private GenderType gender;
    private String address;

    public CaregiverDTO(Integer id, String firstName, String lastName, String birthDate, GenderType gender, String address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public CaregiverDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
