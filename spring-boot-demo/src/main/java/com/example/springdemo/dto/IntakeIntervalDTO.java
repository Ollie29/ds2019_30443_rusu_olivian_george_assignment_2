package com.example.springdemo.dto;

import java.util.Date;

public class IntakeIntervalDTO {

    private Integer id;
    private String startDate;
    private String endDate;

    public IntakeIntervalDTO() {
    }

    public IntakeIntervalDTO(Integer id, String startDate, String endDate) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }
}
